import Solution from "./Solution.js";

export default class __DAY__ extends Solution {
  async prepareData(input: string, isSilver: boolean, isGold: boolean) {
    return input;
  }

  async solveSilver(data: Awaited<ReturnType<__DAY__["prepareData"]>>) {
    return "Not solved Yet";
  }

  async solveGold(data: Awaited<ReturnType<__DAY__["prepareData"]>>) {
    return "Not solved Yet";
  }
}
