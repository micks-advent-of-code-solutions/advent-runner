import fs from "fs/promises";
import path from "path";

abstract class Solution {
  _className: string;

  constructor() {
    this._className = this.constructor.name;

    this.loadInput(this._className)
      .then(this.solve.bind(this))
      .catch((err) => {
        throw err;
      });
  }
  async loadInput(input) {
    const filename = path.resolve("input", input + ".txt");
    try {
      return await fs.readFile(filename, "utf8");
    } catch (err) {
      console.log(err);
      throw new Error("Input file can't be loaded: " + filename);
    }
  }

  async solveTimeWrapper(func, input) {
    const startTime = process.hrtime.bigint();
    const solution = await func.bind(this)(input);
    return {
      duration: "(" + (process.hrtime.bigint() - startTime) / 1000000n + "ms)",
      solution,
    };
  }

  async solve(input) {
    console.log("Solving", this._className);

    const silver = await this.solveTimeWrapper(
      this.solveSilver,
      await this.prepareData(input, true, false),
    );
    const gold = await this.solveTimeWrapper(
      this.solveGold,
      await this.prepareData(input, false, true),
    );

    console.log("[*1]", silver.solution, silver.duration);
    console.log("[*2]", gold.solution, gold.duration);
  }

  abstract prepareData(input: string, isSilver: boolean, isGold: boolean);

  abstract solveSilver(data: Awaited<ReturnType<Solution["prepareData"]>>);

  abstract solveGold(data: Awaited<ReturnType<Solution["prepareData"]>>);
}

export default Solution;
